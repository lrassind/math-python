# Contribuer à math-python

## Comment contribuer

Si vous souhaitez contribuer, suivez ces étapes :

1. Vérifiez les problèmes existants pour vous assurer qu'il n'y a pas déjà un problème similaire signalé ou en cours de résolution.

2. Fork le dépôt math-python sur GitHub.

3. Créez une nouvelle branche pour votre contribution :
   ```bash
   git checkout -b nom-de-votre-branche
   ```

4. Les noms des fonctions seront écrites en minuscule et avec des underscores.
