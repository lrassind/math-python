from unittest import TestCase

from math2.strings import levenshteinDistance


class TestString(TestCase):

    def test_levenshtein(self):
        self.assertEqual(3, levenshteinDistance("kitten", "sitting"))
        self.assertEqual(8, levenshteinDistance("rosettacode", "raisethysword"))
