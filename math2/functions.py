"""
Fonctions mathématiques de base
"""
from typing import List


def sqrt(number: int, tolerance: float =1e-10) -> int:
    if number < 0.0:
        raise ValueError("Square root not defined for negative numbers.")
    guess = number
    while abs(guess * guess - number) >tolerance:
        guess = (guess + number / guess) / 2
    return guess


def average(x: List[float |int]):
    return sum(x)/float(len(x)) if x else 0

from itertools import accumulate, chain
from operator import mul


# factorial :: Integer
def factorial(n):
    return list(
        accumulate(chain([1], range(1, 1 + n)), mul)
    )[-1]
def pgcd(u: int, v: int):
    return pgcd(v, u % v) if v else abs(u)


def median(aray) -> int:
    srtd = sorted(aray)
    alen = len(srtd)
    return 0.5*(   srtd[(alen-1)  //2] + srtd[alen//2])


def pow(x, y):
    return float(x) ** float(y)

from typing import Collection


def fibonacci(until:float) -> Collection[int]:

    n1 = 0
    n2 = 1
    numbers = [n1, n2]

    for loop_count in range(2, int(until)):
        next_term = n1 + n2
        numbers.append(next_term)

        n1, n2 = n2, next_term

    return numbers

def fsum(x: list[float]) -> "Float":
    if len(x) > 0:
        return x[0] + fsum(x[1:])
    return 0

def anagramme(texte: str) -> List[str]:
    dictionnaire_anagrammes = {}

    mots = texte.split()

    for mot in mots:
        mot_trie = ''.join(sorted(mot))

        if mot_trie in dictionnaire_anagrammes:
            dictionnaire_anagrammes[mot_trie].append(mot)
        else:
            dictionnaire_anagrammes[mot_trie] = [mot]

    groupes_anagrammes = list(dictionnaire_anagrammes.values())

    return groupes_anagrammes

def crible_eratosthene(n: int) -> List[int]:
    est_premier = [True] * (n + 1)

    est_premier[0] = est_premier[1] = False

    for i in range(2, int(n**0.5) + 1):
        if est_premier[i]:
            for j in range(i*i, n+1, i):
                est_premier[j] = False

    nombres_premiers = [i for i in range(2, n+1) if est_premier[i]]

    return nombres_premiers
