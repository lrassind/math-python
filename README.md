Léo RASSINDRAME / Julien CHAPUIS

TD GIT pratiques industrie

Partie 1

1/ ```git clone <httpsForkedProject>```
Cette commande nous a permis de cloner le fork du projet du professeur.

2/ ```pytest```
Cette commande nous a permis de tester le code afin de trouver sur quel test il y avait une erreur.

3/```git checkout <nomDeBranche> ```
Cette commande nous a permis de créer un branche sur laquelle travailler

4/ ```git commit -m "Message explicite du commit" ```
Cette commande nous a permis de commit nos changements.

6 & 7/```git bisect <bad> <good> ```
Cette commande nous a permis d'entrer dans le mode bisect entre 1 bon commit et 1 mauvais.
```git blame <nomDuFichier>```
Cette commande nous a permis de voir qui a modifié les lignes du fichier dans le commit dans lequel on est.
```git good``` & ```git bad ```
Ces commandes nous ont permis de dire si le commit est bon ou mauvais. jusqu'à arriver au commit où l'erreur à été introduite.

8/ ```git cherry-pick <commit>``` après un ```git checkout production```
Nous a permis de rajouter un commit précis sur la branche de production pour corriger un bug.

9/ ```git merge``` pour merge la branche de résolution dans le main.

10/ ```git format-patch main -o patches/``` pour créer un dossier "patches" dans lequels un récapitulatif du patch à été créé.



Partie 2

5/ Création de la merge request via gitlab. Nous avons donner un nom à cette merge request, une description, avons mis un reviewer et un Assagnee pour qu'il puisse review le code. Nous avons cocher l'option "squash" pour qu'à la fin du merge, les commit soit squash dans le code.

6 & 7/ review du code par le binome. celui-ci à mis un commentaire par rapport au CONTRIBUTING.md puis a merge les modification puisqu'elle était suffisante

8/ ```git add CONTRIBUTING.md``` pour add le fichier contributing au répository.

Partie 3

1/ Installation de pre-commit ```pip install pre-commit```

2, 3 & 4/ Après configuration du pre-commit, détection et correction des problèmes ```pre-commit run --all-files```



